#!/bin/bash
hadoop fs -mkdir -p input
hdfs dfs -put ./input/* input
yarn jar P22InvertedIndex-0.0.1-SNAPSHOT.jar br.com.projeto22.P22InvertedIndexDriver input output
hdfs dfs -cat output/text-r-00000 >> /output/text.txt
hdfs dfs -cat output/dictionary-r-00000 >> /output/dictionary.txt