package br.com.projeto22;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.log4j.Logger;

public class SecondStepReducer extends Reducer<Text, Text, Text, Text> {

	private static final Logger LOG = Logger.getLogger(P22InvertedIndexDriver.class);
	
	private MultipleOutputs<Text, Text> multipleOutputs;

	HashMap<String, Integer> dictionary = new HashMap<String, Integer>();

	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		this.multipleOutputs = new MultipleOutputs<Text, Text>(context);

		URI[] localPaths = context.getCacheFiles();
		Path path = new Path(localPaths[0]);

		try {
			FileSystem fs = FileSystem.get(context.getConfiguration());
			FSDataInputStream in = fs.open(path);
			InputStreamReader is = new InputStreamReader(in);
			BufferedReader br = new BufferedReader(is);
			String line;
			while ((line = br.readLine()) != null) {
				StringTokenizer tokenizer = new StringTokenizer(line);
				while (tokenizer.hasMoreTokens()) {
					String wordText = tokenizer.nextToken();
					this.dictionary.put(wordText, this.dictionary.size());
				}
			}
		} catch (IOException ioe) {
			LOG.error("############################################### Caught exception while parsing the cached file '" + path);
		}

	}

	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

		Integer index = this.dictionary.get(key.toString());

		List<Integer> docs = new ArrayList<Integer>();
		for (Text val : values) {
			Integer doc = StringUtils.isNumeric(val.toString()) ? Integer.valueOf(val.toString()) : null;
			if (doc != null && !docs.contains(doc)) {
				docs.add(doc);
			}
		}

		Collections.sort(docs);

		this.multipleOutputs.write("text", new Text(index.toString()), new Text(docs.toString()));
		this.multipleOutputs.write("dictionary", key, new Text(index.toString()));
	}
}