package br.com.projeto22;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class FirstStepMapper extends Mapper<LongWritable, Text, Text, Text> {
	
	Text word = new Text();
	Text text = new Text();

	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		
		String line = value
				.toString()
				.replaceAll("[^\\w\\s]"," ")
				.replaceAll("_", "")
				.toLowerCase();

		StringTokenizer tokenizer = new StringTokenizer(line);
		while (tokenizer.hasMoreTokens()) {
			String wordText = tokenizer.nextToken();
			word.set(wordText);
			context.write(word, text);
		}
	}
}