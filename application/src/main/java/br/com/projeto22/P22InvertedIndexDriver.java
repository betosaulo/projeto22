package br.com.projeto22;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

public class P22InvertedIndexDriver extends Configured implements Tool {
	
	private static final Logger LOG = Logger.getLogger(P22InvertedIndexDriver.class);
	
    public static void main(String[] args) throws Exception {
    	ToolRunner.run(new P22InvertedIndexDriver(), args);
    }

	public int run(String[] args) throws Exception {
		
		Path inputFilePath = new Path(args[0]);
		Path outputFilePath = new Path(args[1]);
		
		Path outputTempFilePath = new Path("output_temp");
		
		Configuration conf = getConf();
		
		FileSystem fs = FileSystem.newInstance(conf);

		if (fs.exists(outputTempFilePath)) {
			fs.delete(outputTempFilePath, true);
		}
		
		if (fs.exists(outputFilePath)) {
			fs.delete(outputFilePath, true);
		}
				
		Job jobFirstStep = Job.getInstance(conf);
		
		LOG.info("############ Inicializando o Job 1 - Cria lista de todos os tokens do texto e coloca no cache");
		
		jobFirstStep.setJarByClass(P22InvertedIndexDriver.class);
		
		jobFirstStep.setMapperClass(FirstStepMapper.class);
		jobFirstStep.setReducerClass(FirstStepReducer.class);
		
		jobFirstStep.setInputFormatClass(TextInputFormat.class);
		
		jobFirstStep.setMapOutputKeyClass(Text.class);
		jobFirstStep.setMapOutputValueClass(Text.class);

		jobFirstStep.setOutputKeyClass(Text.class);
		jobFirstStep.setOutputValueClass(Text.class);
		
		jobFirstStep.getConfiguration().set("mapreduce.output.basename", "index");

		FileInputFormat.addInputPath(jobFirstStep, inputFilePath);
		FileOutputFormat.setOutputPath(jobFirstStep, outputTempFilePath);
		
		jobFirstStep.waitForCompletion(true);
		
		Job jobSecondStep = Job.getInstance(conf);
		
		jobSecondStep.addCacheFile(new Path("output_temp", "index-r-00000").toUri());
		
		LOG.info("############ Inicializando o Job 2");
		
		jobSecondStep.setJarByClass(P22InvertedIndexDriver.class);
		
		jobSecondStep.setMapperClass(SecondStepMapper.class);
		jobSecondStep.setReducerClass(SecondStepReducer.class);
		
		jobSecondStep.setInputFormatClass(TextInputFormat.class);
		jobSecondStep.setMapOutputKeyClass(Text.class);
		jobSecondStep.setMapOutputValueClass(Text.class);

		jobSecondStep.setOutputKeyClass(Text.class);
		jobSecondStep.setOutputValueClass(Text.class);

		FileInputFormat.addInputPath(jobSecondStep, inputFilePath);
		FileOutputFormat.setOutputPath(jobSecondStep, outputFilePath);
		
		MultipleOutputs.addNamedOutput(jobSecondStep, "text", TextOutputFormat.class, Text.class, Text.class);
	    MultipleOutputs.addNamedOutput(jobSecondStep, "dictionary", TextOutputFormat.class, Text.class, Text.class);
		
		return jobSecondStep.waitForCompletion(true) ? 0 : 1;
	}
}
