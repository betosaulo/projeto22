#!/bin/bash
mvn -f ./application/pom.xml clean install -T 1C
docker exec -it namenode bash mkdir input && mkdir output
docker cp ./dataset/. namenode:/input/.
docker cp ./application/target/P22InvertedIndex-0.0.1-SNAPSHOT.jar namenode:P22InvertedIndex-0.0.1-SNAPSHOT.jar
docker cp ./run.sh namenode:run.sh
docker exec -it namenode bash ./run.sh
docker cp namenode:/output/text.txt ./output/text.txt
docker cp namenode:/output/dictionary.txt ./output/dictionary.txt